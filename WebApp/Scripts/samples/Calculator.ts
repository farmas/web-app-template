///<reference path="../typings/TypeDefinitions.d.ts" />

export = Main;

module Main {
    export class Calculator {
        public add(x: number, y: number): number {
            return x + y;
        }
    }
}
