﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Farmas.WebApp.Controllers
{
    public class ResourcesController: ApiController
    {
        protected HttpResponseMessage GetResponseMessage(string path)
        {
            var manifestResourceName = String.Format("Farmas.WebApp.{0}", path);
            var stream = typeof(ResourcesController).Assembly
                .GetManifestResourceStream(manifestResourceName);

            var response = this.Request.CreateResponse();
            response.Content = new StreamContent(stream);

            return response;
        }

       
    }
}
