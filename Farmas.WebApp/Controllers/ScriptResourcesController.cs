﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Farmas.WebApp.Controllers
{
    public class ScriptResourcesController: ResourcesController
    {
        public HttpResponseMessage Get(string path)
        {
            var response = this.GetResponseMessage("Scripts." + path);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/javascript");
            return response;
        }
    }
}
