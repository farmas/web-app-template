﻿# Based on: http://nuget.codeplex.com/discussions/254095

param($installPath, $toolsPath, $package, $project)

$typeScriptTargesPath = "`$(MSBuildExtensionsPath32)\Microsoft\VisualStudio\v`$(VisualStudioVersion)\TypeScript\Microsoft.TypeScript.targets"

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.Build") | out-null
$msbuild = [Microsoft.Build.Evaluation.ProjectCollection]::GlobalProjectCollection.GetLoadedProjects($project.FullName) | Select-Object -First 1

Write-Host "Adding TypeScript PropertyGroup"
$typeScriptPropertyGroup = $msbuild.Xml.AddPropertyGroup()
$typeScriptPropertyGroup.AddProperty("TypeScriptTarget", "ES5")
$typeScriptPropertyGroup.AddProperty("TypeScriptIncludeComments", "true")
$typeScriptPropertyGroup.AddProperty("TypeScriptModuleKind", "AMD")

Write-Host "Updating *.ts items to TypeScriptCompile"
$items = $msbuild.Xml.Items | ? { $_.Include.EndsWith(".d.ts") -eq $false } | ? { $_.Include.EndsWith(".ts") -eq $true }
$items | % { $_.ItemType = "TypeScriptCompile" }

Write-Host "Adding TypeScript .targets import"
$msbuild.Xml.AddImport($typeScriptTargesPath) | out-null

$project.Save()