﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Routing;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Farmas.WebApp.AppStart), "Initialize")]

namespace Farmas.WebApp
{
    public static class AppStart
    {
        public static void Initialize()
        {
            ConfigureRoutes(GlobalConfiguration.Configuration);
        }

        private static void ConfigureRoutes(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "ScriptsApi",
                routeTemplate: "resources/scripts/{path}",
                defaults: new { controller = "ScriptResources" }
            );
        }
    }
}
