﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Farmas.WebApp
{
    public static class HttpContextExtensions
    {
        public static bool IsLocal(this HttpContextBase context)
        {
            return context.Request.Url.Host.Equals("localhost", StringComparison.OrdinalIgnoreCase);
        }
    }
}
