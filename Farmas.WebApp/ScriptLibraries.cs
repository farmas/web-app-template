﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmas.WebApp
{
    public class ScriptLibrary
    {
        public string CdnUrl { get; set; }
        public string LocalVirtualUrl { get; set; }
        public string ScriptTest { get; set; }
    }

    public static class ScriptLibraries
    {
        public static ScriptLibrary JQuery 
        {
            get
            {
                return new ScriptLibrary()
                {
                    CdnUrl = "//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js",
                    LocalVirtualUrl = "~/resources/scripts/jquery-2.0.3.min.js",
                    ScriptTest = "window.jQuery"
                };
            }
        }

        public static ScriptLibrary Knockout
        {
            get
            {
                return new ScriptLibrary()
                {
                    CdnUrl = "http://ajax.aspnetcdn.com/ajax/knockout/knockout-3.0.0.js",
                    LocalVirtualUrl = "~/resources/scripts/knockout-3.0.0.js",
                    ScriptTest = "window.ko"
                };
            }
        }

        public static ScriptLibrary Underscore
        {
            get
            {
                return new ScriptLibrary()
                {
                    LocalVirtualUrl = "~/resources/scripts/underscore.min.js",
                    ScriptTest = "_"
                };
            }
        }
    }
}
