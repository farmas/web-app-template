﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace Farmas.WebApp
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString InitializeWebApp(this HtmlHelper htmlHelper, string mainModule, params ScriptLibrary[] scriptLibraries)
        {
            var stringBuilder = new StringBuilder();
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            foreach (var scriptLibrary in scriptLibraries)
            {
                stringBuilder.AppendLine(GetScriptLibraryString(scriptLibrary, urlHelper));
            }

            var requireJsElement = new XElement("script",
                new XAttribute("data-main", HttpUtility.HtmlAttributeEncode(mainModule)),
                new XAttribute("src", urlHelper.Content("~/resources/scripts/require.js"))
            );
            requireJsElement.SetValue("");
            stringBuilder.AppendLine(requireJsElement.ToString());

            return new HtmlString(stringBuilder.ToString());
        }

        private static string GetScriptLibraryString(ScriptLibrary scriptLibrary, UrlHelper urlHelper)
        {
            var stringBuilder = new StringBuilder();

            if (!String.IsNullOrEmpty(scriptLibrary.CdnUrl) && !urlHelper.RequestContext.HttpContext.IsLocal())
            {
                var cdnElement = new XElement("script", new XAttribute("src", scriptLibrary.CdnUrl));
                cdnElement.SetValue("");
                stringBuilder.AppendLine(cdnElement.ToString());
            }
            
            stringBuilder.AppendLine(String.Format(
                "<script>{0} || document.write('<script src=\"{1}\"><\\/script>')</script>",
                scriptLibrary.ScriptTest,
                urlHelper.Content(scriptLibrary.LocalVirtualUrl)));

            return stringBuilder.ToString();
        }
    }
}
